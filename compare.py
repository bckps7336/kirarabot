#!/usr/bin/python

import cv2

import numpy as np

si = ""
li = ""

while True:
    _si = raw_input("Small Image" + ("[" + si + "]" if si != "" else "") + ": ")
    if _si != "":
        si = _si

    _li = raw_input("Large Image" + ("[" + li + "]" if li != "" else "") + ": ")
    if _li != "":
        li = _li

    small_image = cv2.imread(si)
    large_image = cv2.imread(li)

    h, w = small_image.shape[:-1]

    res = cv2.matchTemplate(large_image, small_image, cv2.TM_CCOEFF_NORMED)
    print res

    mV,MV,mLoc,MLoc = cv2.minMaxLoc(res)

    print "Accuracy: " + str(100 if mV == 0 else 1 / mV / MV / 100000) + "%"

    threshold = .8
    loc = np.where(res >= threshold)

    print "Matching points: " + str(len(loc))
    for pt in zip(*loc[::-1]):  # Switch collumns and rows
        print pt
        cv2.rectangle(large_image, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)

    cv2.imshow('output',large_image)

    # The image is only displayed if we call this
    cv2.waitKey(0)
