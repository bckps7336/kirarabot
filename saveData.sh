#!/bin/sh

source="/storage/emulated/legacy/Android/data/com.aniplex.kirarafantasia/files/"

save_dir="/storage/emulated/legacy/kirara/"

save_dir=$save_dir$RANDOM"/"

files="a.d a.d2 s.d s.d2"

comment=$@

am kill com.aniplex.kirarafantasia

mkdir $save_dir

for i in $files; do
    mv "$source$i" "$save_dir";
    echo $i

    echo $comment > $save_dir"comment.txt"
done
