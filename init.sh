#!/bin/sh

INSTALL_DIR="/sdcard/"

echo "Installing script to $INSTALL_DIR..."
adb push saveData.sh $INSTALL_DIR

#adb shell su -c "/sdcard/saveData.sh"

[ $(adb shell ls $INSTALL_DIR | grep saveData.sh) ] && echo "Script installed." || echo "Script not found! Please check directory write access."

echo "Done."
