#!/usr/bin/python

import cv2
import numpy as np

i = ""

starColor = {
    "3": ([80, 95, 149], [100, 115, 230]), # 3
    "4": ([113, 103, 75], [133, 123, 155]), # 4
    "5": ([0, 150, 180], [10, 190, 255]), # 5
    "3*": ([30, 40, 50], [55, 65, 130]) # 3*
    # 4*
    # 5*
}

"""
    Row 1: (510, 355)
    Row 2: (510, 495)

    Icon: 122 x 122
    Height: 25
    Padding: 20
"""



while True:
    result = [
        [], [], [], [], [],
        [], [], [], [], []
    ]

    _i = raw_input("Image" + ("[" + i + "]" if i != "" else "") + ": ")
    if _i != "":
        i = _i
        img_rgb = cv2.imread(i)
        rows = []
        for j in range(0, 2):
            rows.append([])
            for k in range(0, 5):
                rows[j].append(img_rgb[350 + 145 * j:350 + 145 * j + 22, 509 + (122 + 19) * k:509 + (122 + 19) * k + 122])

    for s, (l, u) in starColor.iteritems():
        low = np.array(l, dtype="uint8")
        up = np.array(u, dtype="uint8")
        print "Star " + s + ":"

        count = 1

        for row in rows:
            for item in row:
                mask = cv2.inRange(item, low, up)
                # mask = 2D array
                output = cv2.bitwise_and(item, item, mask = mask)

                print "\tColor count of " + str(count) + ": " + str(float(np.sum(output)) / 255.0 / float(item.size) * 100.0) + "%"

                result[count - 1].append(float(np.sum(output)) / 255.0 / float(item.size) * 100.0)

                count = count + 1

    result = map(lambda x: list(starColor.keys())[x.index(max(x))], result)

    print result
