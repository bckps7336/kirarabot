#!/usr/bin/python

from random import randint, choice
import string

import pyscreenshot as ig
import cv2
import os
import sys

import numpy as np
import time
import math

import adbLib


scrImg = "";

starColor = {
    "3": ([80, 95, 149], [100, 115, 230]), # 3
    "4": ([113, 103, 75], [133, 123, 155]), # 4
    "5": ([0, 150, 180], [10, 190, 255]), # 5
    "3*": ([30, 40, 50], [55, 65, 130]) # 3*
    # 4*
    # 5*
}

condition = {"3": 0, "4": 0, "5": 1}


logFile = "log/" + str(int(time.time())) + ".log"

def ranID(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(choice(chars) for _ in range(size))

def report(string):
    txt = time.strftime("%c") + "\t" + string
    print txt
    with open(logFile, "a+") as f:
        f.write(txt + "\n")

def remove():
    global scrImg

    s = os.unlink(scrImg)
    scrImg = scrImg = "tmp/" + ranID() + ".png"
    return s

def clickRandom(x1 = None, y1 = None, d1 = None, d2 = None):
    rect = adbLib.getRect()

    if x1 == None:
        x1 = 60
    if y1 == None:
        y1 = 60
    if d1 == None:
        d1 = rect[0] - 60
    if d2 == None:
        d2 = rect[1] - 60

    return adbLib.click(randint(x1, x1 + d1), randint(y1, y1 + d2))

def matchImage(hay, needle):
    global k

    # Read the images from the file
    small_image = cv2.imread(needle)
    large_image = cv2.imread(hay)

    if k != 1:
        large_image = cv2.resize(large_image, None, fx=k, fy=k, interpolation = cv2.INTER_CUBIC)
    
    result = cv2.matchTemplate(small_image, large_image, cv2.TM_SQDIFF_NORMED)

    # We want the minimum squared difference
    mV,MV,mLoc,MLoc = cv2.minMaxLoc(result)

    #print (mV,MV,mLoc,MLoc)
    # Extract the coordinates of our best match
    if mV < 0.0001:
        return mLoc
    else:
        return None

def waitImage(hay, needle, randClick = False):
    pos = matchImage(hay, needle)
    while pos == None:
        remove()
        if randClick:
            clickRandom()

        time.sleep(randint(200, 500) / 1000.0)
        adbLib.screenshot(scrImg)

        hay = scrImg
        if matchImage(hay, "image/errorComm.png"):
            report("Communication error found! Resolving...")
            okPos = matchImage(hay, "image/ok.png")
            clickRandom(okPos[0] - 20, okPos[1], 50, 20)
        else:
            pos = matchImage(hay, needle)
    return pos

def parseStar(scrImg):
    global starColor

    result = [
        [], [], [], [], [],
        [], [], [], [], []
    ]

    adbLib.screenshot(scrImg)

    img_rgb = cv2.imread(scrImg)

    rows = []
    for j in range(0, 2):
        rows.append([])
        for k in range(0, 5):
            rows[j].append(img_rgb[329 + 145 * j:329 + 145 * j + 22, 509 + (122 + 19) * k:509 + (122 + 19) * k + 122])


    for s, (l, u) in starColor.iteritems():
        low = np.array(l, dtype="uint8")
        up = np.array(u, dtype="uint8")

        count = 1

        for row in rows:
            for item in row:
                mask = cv2.inRange(item, low, up)

                output = cv2.bitwise_and(item, item, mask = mask)

                result[count - 1].append(float(np.sum(output)) / 255.0 / float(item.size) * 100.0)

                count = count + 1

    result = map(lambda x: list(starColor.keys())[x.index(max(x))], result)
    return result

if __name__ == '__main__':
    scrImg = "tmp/" + ranID() + ".png"
    screenRes = adbLib.getRect()
    k = 1280 / screenRes[0]
    if k != 720 / screenRes[1]:
        raise Exception("Internal Error")

    report("App started.")
    report("Finding target device...")

    if adbLib.checkDevice() == None:
        report("Target device not found!")
        os._exit(0)

    runCount = 0

    while True:
        runCount = runCount + 1
        report("Starting run " + str(runCount))

        adbLib.screenshot(scrImg)

        report("Cleaning up...")
        adbLib.shell_command("input", "keyevent", "3")
        adbLib.shell_command("am", "kill", "com.aniplex.kirarafantasia")

        report("Starting app...")
        adbLib.shell_command("monkey -p com.aniplex.kirarafantasia 1")

        report("Waiting for title screen...")
        iconPos = waitImage(scrImg, "image/title.png", True) #
        report("Found title screen at " + str(iconPos))

        if matchImage(scrImg, "image/titleNewAC.png"):
            report("New account detected.")

            report("Entering...")
            clickRandom(y1 = 150, d1 = 800, d2 = 300)

            report("Waiting for account creation screen...")
            loaded = matchImage(scrImg, "image/usernameScreen.png")

            while loaded == None:
                remove()

                time.sleep(randint(200, 500) / 500.0)
                adbLib.screenshot(scrImg)

                if matchImage(scrImg, "image/dataScreen.png"):
                    report("Data download screen detected.")
                    okPos = matchImage(scrImg, "image/ok.png")
                    clickRandom(okPos[0] - 20, okPos[1], 50, 20)
                elif matchImage(scrImg, "image/ToSScreen.png"):
                    report("Found ToS screen.")
                    tosOKPos = matchImage(scrImg, "image/ToSAccept.png")
                    clickRandom(tosOKPos[0], tosOKPos[1], 100, 25)
                elif matchImage(scrImg, "image/errorComm.png"):
                    report("Communication error found! Resolving...")
                    okPos = matchImage(scrImg, "image/ok.png")
                    clickRandom(okPos[0] - 20, okPos[1], 50, 20)
                else:
                    loaded = matchImage(scrImg, "image/usernameScreen.png")

            report("Account creation screen detected.")
            inputPos = matchImage(scrImg, "image/usernameInput.png")
            clickRandom(inputPos[0], inputPos[1], 300, 30)

            username = ranID()
            adbLib.shell_command("input", "text", username)

            confirmPos = waitImage(scrImg, "image/usernameConfirm.png")
            clickRandom(confirmPos[0], confirmPos[1], 150, 30)
            clickRandom(confirmPos[0], confirmPos[1], 150, 30)


            loaded = matchImage(scrImg, "image/presentScreen.png")
            while loaded == None:
                remove()

                time.sleep(randint(200, 500) / 500.0)
                adbLib.screenshot(scrImg)

                if matchImage(scrImg, "image/titleNewAC.png"):
                    report("Invalid username" + username + "!")
                    os.execl(sys.executable, * sys.argv)
                else:
                    loaded = matchImage(scrImg, "image/usernameScreen.png")

            report("Retrieving presents...")
            okPos = waitImage(scrImg, "image/ok.png")
            clickRandom(okPos[0] - 20, okPos[1], 100, 25)

            receiveButton = waitImage(scrImg, "image/presentReceive.png")
            clickRandom(receiveButton[0], receiveButton[1], 150, 30)

            waitImage(scrImg, "image/presentDone.png")
            closePos = waitImage(scrImg, "image/tojiru.png")
            clickRandom(closePos[0], closePos[1], 150, 30)

            skipDrawer = waitImage(scrImg, "image/dialogSkipDrawer.png")
            report("Skipping the dialogue...")
            clickRandom(skipDrawer[0] + 10, skipDrawer[1] + 10, 50, 50)

            skipButton = waitImage(scrImg, "image/skipButton.png")
            clickRandom(skipButton[0], skipButton[1], 130, 25)

            skipConfirm = waitImage(scrImg, "image/dialogSkipConfirm.png")
            clickRandom(skipButton[0], skipButton[1], 150, 30)

            waitImage(scrImg, "image/gachaScreen.png")
            report("Gacha preparation screen detected.")
            okPos = waitImage(scrImg, "image/ok.png")
            clickRandom(okPos[0] - 20, okPos[1], 100, 25)

            gachaStart = waitImage(scrImg, "image/gachaStart.png")
            clickRandom(gachaStart[0] + 20, gachaStart[1] + 20, 200, 40)

            gachaConfirm = waitImage(scrImg, "image/gachaConfirmOK.png")
            clickRandom(gachaConfirm[0], gachaConfirm[1], 150, 30)

            gachaLoaded = waitImage(scrImg, "image/gachaWaitTap.png")
            clickRandom()

            gachaSplashSkip = waitImage(scrImg, "image/skipButton2.png")
            clickRandom(gachaSplashSkip[0], gachaSplashSkip[1], 120, 25)

            loaded = False
            while not loaded:
                clickRandom()
                time.sleep(randint(50, 300) / 1000.0)

                remove()
                adbLib.screenshot(scrImg)

                loaded = waitImage(scrImg, "image/gachaResult.png")

            report("Gacha screen detected.")
        else:
            report("Entering...")
            clickRandom(y1 = 150, d1 = 800, d2 = 300)

            report("Waiting for gacha screen...")
            loaded = matchImage(scrImg, "image/gachaResult.png")

            while loaded == None:
                remove()

                time.sleep(randint(200, 500) / 500.0)
                adbLib.screenshot(scrImg)

                if matchImage(scrImg, "image/dataScreen.png"):
                    report("Data download screen detected.")
                    okPos = matchImage(scrImg, "image/ok.png")
                    clickRandom(okPos[0] - 20, okPos[1], 50, 20)
                elif matchImage(scrImg, "image/errorComm.png"):
                    report("Communication error found! Resolving...")
                    okPos = matchImage(scrImg, "image/ok.png")
                    clickRandom(okPos[0] - 20, okPos[1], 50, 20)
                else:
                    loaded = matchImage(scrImg, "image/gachaResult.png")

            report("Gacha screen detected.")

        while True:
            cardRating = parseStar(scrImg)
            report("Card status: " + str(cardRating))
            report("Matching with condition " + str(condition))

            satisfied = True
            got = {"3": 0, "4": 0, "5": 0}
            for s, r in condition.iteritems():
                tmp = map(lambda x: s in x, cardRating).count(True)
                got[s] = tmp
                if tmp < r:
                    satisfied = False
            report(("" if satisfied else "Not ") + "Satisfied with " + str(got))

            if satisfied:
                break

            report("Requesting re-gacha...")
            regachaPos = waitImage(scrImg, "image/tojiru2.png")
            clickRandom(regachaPos[0], regachaPos[1], 100, 30)

            regachaConfirmPos = waitImage(scrImg, "image/redraw.png")
            clickRandom(regachaConfirmPos[0], regachaConfirmPos[1], 160, 25)

            waitImage(scrImg, "image/gachaWaitTap.png", True)

            gachaSplashSkip = waitImage(scrImg, "image/skipButton2.png", True)
            clickRandom(gachaSplashSkip[0], gachaSplashSkip[1], 120, 25)

            report("Waiting for the end of introduction...")
            loaded = False
            while not loaded:
                remove()
                adbLib.screenshot(scrImg)

                loaded = matchImage(scrImg, "image/gachaResult.png")

                if loaded:
                    break

                clickRandom()
                time.sleep(randint(50, 300) / 1000.0)
            report("Gacha screen detected.")

        adbLib.screenshot(str(int(time.time())) + ".png")
        adbLib.shell_command("input", "keyevent", "3")
        adbLib.shell_command("am", "kill", "com.aniplex.kirarafantasia")
        adbLib.shell_command("sh", "/storage/emulated/legacy/saveData.sh", "Matching condition: " , str(condition))
