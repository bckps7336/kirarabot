#!/usr/bin/python

import subprocess

from random import choice
import string

import re

import config

import distutils.spawn

host = config.host

if "adb" in vars(config):
    adb = config.adb
else:
    adb = distutils.spawn.find_executable("adb")
    if adb is None:
        raise Exception("adb not found. Please specify adb executable in config.py.")

def adb_command(*args):
    process = subprocess.Popen([adb] + list(args), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    return process.communicate()

if "Android Debug Bridge" not in adb_command()[1]:
    raise Exception("Executable is not ADB")

if "pos" in vars(config):
    pos = config.pos
    script = pos  + "/saveData.sh"
else:
    pos = "/sdcard/"
    script = "/sdcard/saveData.sh"

def shell_command(*args):
    checkDevice()
    return adb_command("shell", *args)

def connect():
    status, _ = adb_command("connect", host)

    if "connected" in status:
        return True
    raise Exception("Could not connect to host " + host)

def checkDevice():
    if "." in host:
        connect()
    status, _ = adb_command("devices")
    if str(host + "\tdevice") in status:
        return True
    elif str(host) in status:
        raise Exception("Please authorize your phone first.")
    raise Exception("Device " + host + " not found. Check cable connection.")

if "No such file" in shell_command("ls", script):
    raise Exception("Script file not found. Please run init.sh.")


def screenshot(path = None):
    def ranID(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(choice(chars) for _ in range(size))

    filename = pos + ranID() + ".png"

    shell_command('screencap', '-p', filename)

    if path != None:
        adb_command('pull', filename, path)
    else:
        adb_command('pull', filename)

    shell_command('rm', filename)

def click(x, y):
    return shell_command("input", "tap", str(x), str(y))

def getRect():
    out, _ = shell_command("wm", "size")

    match = re.match(r'Physical size: (\d+)x(\d+)', out)
    if match == None:
        raise Exception("Screen size could not be found through wm")
    else:
        out = tuple(map(lambda x: int(x), sorted(list(match.groups()), reverse=True)))
        ratio = out[0] / out[1]
        if ratio != 16 / 9 and ratio != 9 / 16:
            raise Exception("Only 16:9 ratio devices are supported, got " + str(out))
        return out

if __name__ == "__main__":
    screenshot()
